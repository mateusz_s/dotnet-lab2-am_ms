﻿using System;
using System.Diagnostics;

namespace gol
{
    public class World
    {
        private int[,] _map;

        public World(int height, int width)
        {
            _map = new int[height, width];
            RandomizeMap();
        }

        public World(int height, int width, int[,] map)
        {
            this._map = new int[height, width];
            for (var h = 0; h < height; h++)
            {
                for (var w = 0; w < width; w++)
                {
                    this._map[h, w] = map[h, w];
                }
            }
        }

        public int[,] GetMap()
        {
            return _map;
        }

        private void RandomizeMap()
        {
            var rand = new Random();
            for (var h = 0; h < _map.GetLength(0); h++)
            {
                for (var w = 0; w < _map.GetLength(1); w++)
                {
                    _map[h, w] = rand.Next()%2;
                }
            }
        }

        public void PrintMap()
        {
            for (var h = 0; h < _map.GetLength(0); h++)
            {
                for (var w = 0; w < _map.GetLength(1); w++)
                {
                    Console.Write(_map[h, w] + " ");
                }
                Console.Write("\n");
            }
        }

        public int CountNeighbours(int heightPos, int widthPos)
        {
            var nrOfNeighbours = 0;
            nrOfNeighbours -= _map[heightPos, widthPos];
            var decreaseHeightPos = 0;
            var decreaseWidthPos = 0;

            if (heightPos == 0)
            {
                heightPos++;
                decreaseHeightPos = 1;
            }
            else if (heightPos == _map.GetLength(0) - 1)
            {
                decreaseHeightPos = 1;
            }
            if (widthPos == 0)
            {
                widthPos++;
                decreaseWidthPos = 1;
            }
            else if (widthPos == _map.GetLength(1) - 1)
            {
                decreaseWidthPos = 1;
            }

            for (var h = heightPos - 1; h <= heightPos + 1 - decreaseHeightPos; h++)
            {
                for (var w = widthPos - 1; w <= widthPos + 1 - decreaseWidthPos; w++)
                {
                    nrOfNeighbours += _map[h, w];
                }
            }

            return nrOfNeighbours;
        }

        public void NextIteration()
        {
            var nextMap = new int[_map.GetLength(0), _map.GetLength(1)];

            for (var h = 0; h < _map.GetLength(0); h++)
            {
                for (var w = 0; w < _map.GetLength(1); w++)
                {
                    var nrOfNghrbs = CountNeighbours(h, w);
                    if (_map[h, w] == 1)
                    {
                        if (nrOfNghrbs < 2 || nrOfNghrbs > 3)
                        {
                            nextMap[h, w] = 0;
                        }
                        else
                        {
                            nextMap[h, w] = 1;
                        }
                    }
                    else
                    {
                        if (nrOfNghrbs == 3)
                        {
                            nextMap[h, w] = 1;
                        }
                        else
                        {
                            nextMap[h, w] = 0;
                        }
                    }
                }
            }
            for (var i = 0; i < _map.GetLength(0); i++)
            {
                for (var j = 0; j < _map.GetLength(1); j++)
                {
                    _map[i, j] = nextMap[i, j];
                }
            }
            if (!Console.IsOutputRedirected) Console.Clear();

            PrintMap();
        }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            var myAwesomeWorld = new World(10, 10);
            ConsoleKeyInfo keyInfo;
            myAwesomeWorld.PrintMap();
            do
            {
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Enter)
                {
                    myAwesomeWorld.NextIteration();
                }
            } while (keyInfo.Key != ConsoleKey.Escape);
        }
    }
}