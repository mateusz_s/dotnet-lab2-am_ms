﻿using System;
using System.IO;

namespace gol
{
    using NUnit.Framework;

    [TestFixture]
    public class WorldTests
    {
        [Test]
        public void Map3x3_NooneDiesWith2NeighboursOneBornWith3()
        {
            int[,] map = new int[3, 3];
            map[0, 0] = 1;
            map[0, 1] = 1;
            map[1, 1] = 1;

            int[,] expectedMap = new int[3, 3];
            expectedMap[0, 0] = 1;
            expectedMap[0, 1] = 1;
            expectedMap[1, 0] = 1;
            expectedMap[1, 1] = 1;

            World world = new World(3, 3, map);
            world.NextIteration();

            Assert.AreEqual(expectedMap, world.GetMap());

        }
        [Test]
        public void Map3x3_NothingChangesWith3Neighbours()
        {
            int[,] map = new int[3, 3];
            map[0, 0] = 1;
            map[0, 1] = 1;
            map[1, 0] = 1;
            map[1, 1] = 1;


            int[,] expectedMap = new int[3, 3];
            expectedMap[0, 0] = 1;
            expectedMap[0, 1] = 1;
            expectedMap[1, 0] = 1;
            expectedMap[1, 1] = 1;


            World world = new World(3, 3, map);
            world.NextIteration();

            Assert.AreEqual(expectedMap, world.GetMap());

        }
        [Test]
        public void Map3x3_DieWhenLessThan2Neighbours()
        {
            int[,] map = new int[3, 3];
            map[0, 1] = 1;
            map[1, 1] = 1;

            int[,] expectedMap = new int[3, 3];

            World world = new World(3, 3, map);
            world.NextIteration();

            Assert.AreEqual(expectedMap, world.GetMap());

        }

        [Test]
        public void Map3x4_GetNrOfNghbrsInLeftTopCorner()
        {
            int[,] map = new int[3, 4];
            map[0, 1] = 1;
            map[1, 0] = 1;
            map[0, 0] = 1;

            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(0, 0);

            Assert.AreEqual(2, nrOfNeighbours);

        }

        [Test]
        public void Map3x4_GetNrOfNghbrsInBottomLeftCorner()
        {
            int[,] map = new int[3, 4];
            map[2, 1] = 1;
            map[2, 0] = 1;
            map[1, 0] = 1;

            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(2, 0);

            Assert.AreEqual(2, nrOfNeighbours);

        }

        [Test]
        public void Map3x4_GetNrOfNghbrsInRightTopCorner()
        {
            int[,] map = new int[3, 4];
            map[0, 3] = 1;
            map[1, 3] = 1;
            map[1, 2] = 1;

            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(0, 3);

            Assert.AreEqual(2, nrOfNeighbours);

        }
        [Test]
        public void Map3x4_GetNrOfNghbrsInDownBottomCorner()
        {
            int[,] map = new int[3, 4];
            map[2, 3] = 1;
            map[2, 2] = 1;
            map[1, 3] = 1;
            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(2, 3);

            Assert.AreEqual(2, nrOfNeighbours);

        }
        [Test]
        public void Map3x4_GetNrOfNghbrsLeftEdge()
        {
            int[,] map = new int[3, 4];
            map[0, 0] = 1;
            map[0, 1] = 1;
            map[1, 1] = 1;
            map[1, 0] = 1;

            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(1, 0);

            Assert.AreEqual(3, nrOfNeighbours);

        }
        [Test]
        public void Map3x4_GetNrOfNghbrsRightEdge()
        {
            int[,] map = new int[3, 4];
            map[0, 3] = 1;
            map[2, 2] = 1;
            map[1, 3] = 1;
            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(1, 3);

            Assert.AreEqual(2, nrOfNeighbours);

        }
        [Test]
        public void Map3x4_GetNrOfNghbrsTopEdge()
        {
            int[,] map = new int[3, 4];
            map[0, 0] = 1;
            map[0, 2] = 1;
            map[1, 1] = 1;
            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(0, 1);

            Assert.AreEqual(3, nrOfNeighbours);

        }
        [Test]
        public void Map3x4_GetNrOfNghbrsBottomEdge()
        {
            int[,] map = new int[3, 4];
            map[2, 2] = 1;
            map[2, 0] = 1;
            map[1, 1] = 1;
            World world = new World(3, 4, map);
            int nrOfNeighbours = world.CountNeighbours(2, 1);

            Assert.AreEqual(3, nrOfNeighbours);

        }
        [Test]
        public void Map3x3_GetNrOfNghbrsCentral()
        {
            int[,] map = new int[3, 3];
            map[0, 1] = 1;
            map[2, 0] = 1;
            map[1, 2] = 1;
            World world = new World(3, 3, map);
            int nrOfNeighbours = world.CountNeighbours(1, 1);

            Assert.AreEqual(3, nrOfNeighbours);
        }
        
    }
}