﻿using System;
using System.IO;

namespace gol
{
    using NUnit.Framework;

    [TestFixture]
    public class WorldTests
    {
        [Test]
        public void Map3x3_NooneDiesWith2NeighboursOneBornWith3()
        {
            int[,] map = new int[3, 3];
            map[0, 0] = 1;
            map[0, 1] = 1;
            map[1, 1] = 1;

            int[,] expectedMap = new int[3, 3];
            expectedMap[0, 0] = 1;
            expectedMap[0, 1] = 1;
            expectedMap[1, 0] = 1;
            expectedMap[1, 1] = 1;

            World world = new World(3, 3, map);
            world.nextIteration();

            Assert.AreEqual(expectedMap, world.getMap());

        }
        [Test]
        public void Map3x3_NothingChangesWith3Neighbours()
        {
            int[,] map = new int[3, 3];
            map[0, 0] = 1;
            map[0, 1] = 1;
            map[1, 0] = 1;
            map[1, 1] = 1;


            int[,] expectedMap = new int[3, 3];
            expectedMap[0, 0] = 1;
            expectedMap[0, 1] = 1;
            expectedMap[1, 0] = 1;
            expectedMap[1, 1] = 1;


            World world = new World(3, 3, map);
            world.nextIteration();

            Assert.AreEqual(expectedMap, world.getMap());

        }
        [Test]
        public void Map3x3_DieWhenLessThan2Neighbours()
        {
            int[,] map = new int[3, 3];
            map[0, 1] = 1;
            map[1, 1] = 1;

            int[,] expectedMap = new int[3, 3];

            World world = new World(3, 3, map);
            world.nextIteration();

            Assert.AreEqual(expectedMap, world.getMap());

        }
    }
}